package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;


public class GameScreen extends ScreenAdapter {
	private InvisibleMazeBattleGame invisibleMazeBatteGame;
	private Wizard wizard;
	private WorldRenderer worldRenderer;
	World world;
	
	public GameScreen(InvisibleMazeBattleGame invisibleMazeBatteGame) {
        this.invisibleMazeBatteGame = invisibleMazeBatteGame;
        world = new World(invisibleMazeBatteGame);
        wizard = world.getWizard();
        worldRenderer = new WorldRenderer(invisibleMazeBatteGame , world);
    }
	
	public void update(float delta){
		world.update(delta);
	}
	
	 @Override
	 public void render(float delta) {
		 SpriteBatch batch  = invisibleMazeBatteGame.batch;
		 world.update(delta);
		 update(delta);
	     Gdx.gl.glClearColor(0, 0, 0, 1);
	     Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	     worldRenderer.render(delta);
	 }
}
