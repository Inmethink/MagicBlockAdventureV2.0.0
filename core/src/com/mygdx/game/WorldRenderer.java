package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class WorldRenderer {
	private InvisibleMazeBattleGame invisibleMazeBatteGame;
	private Wizard wizard;
	World world;
	public SpriteBatch batch;
	private Texture wizardImg;
	public BlockRenderer blockRenderer;
	public static final int BLOCK_SIZE = 40;
	
	public WorldRenderer(InvisibleMazeBattleGame invisibleMazeBatteGame , World world) {
		this.invisibleMazeBatteGame = invisibleMazeBatteGame;
		batch = invisibleMazeBatteGame.batch;
		blockRenderer = new BlockRenderer(invisibleMazeBatteGame.batch, world);
		this.world = world;
		wizard = world.getWizard();
		wizardImg = new Texture("WizardInGame1.png");
	}
	
	 public void render(float delta) {
		 SpriteBatch batch = invisibleMazeBatteGame.batch;
		 Vector2 pos = world.getWizard().getPosition();
		 batch.begin();
		 blockRenderer.render();
	     batch.draw(wizardImg, pos.x - BLOCK_SIZE/2, InvisibleMazeBattleGame.HEIGHT - pos.y - BLOCK_SIZE/2);
	     batch.end();
	 }

}
