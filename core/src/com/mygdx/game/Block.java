
package com.mygdx.game;



public class Block {

    private String[][] MAP = new String [][] {
    	{
            "########################################",
            "#......................#.....#..#....AAA",
            "#.###.###..###.###.#.##.#.##..#.####.###",
            "#.#...#......#...#.#.#.....##.#....#.#.#",
            "#.#.###.####.###.#.#...#.#..#..###.#.#.#",
            "#.#.#..........#.#.#.#.#..####.....#.#.#",
            "#.....###..###.....#.#..#...#######..#.#",
            "#.#.#..........#.#.#.#.####.#######..#.#",
            "#.#.###.####.###.#.#...##...##...##.##.#",
            "#.#...#......#...#...#######......#....#",
            "#.###.###..###.###.#.....##..#....#.####",
            "#..................#.###......###......#",
            "########################################" 
    	},
    	{
    		 "########################################",
             "#...#...#..............#.....#..#....AAA",
             "#S###.########.###.#.##.#.##..#.####.###",
             "#.#...#..#...#...#.#.#.....##.#....#.#.#",
             "#.#.###.####.###.####...#.#..#..####.#.#",
             "#.#.#......#...#.#.#.#.#..####.....#.#.#",
             "#.....###..###.....#.#..#...#######..#.#",
             "#.#.#..#.......#.#.#.#.####.#######..#.#",
             "#.#.###.####.###.#.#...##...##...##.##.#",
             "#.#...#......#...#...#######......#....#",
             "#.###.###..###.###.#.....##..#....#.####",
             "#..#.........#.....#.###......###......#",
             "########################################" 
    	}
    };
    
    private int numberOfMap = 1;
    private int height;
    private int width;
    private int spawnX;
    private int spawnY;
    private char[][] CharMAP = new char[42][40];

    public Block() {
        height = MAP[numberOfMap].length;
        width = MAP[numberOfMap][0].length();
    }
  
    public int getHeight() {
        return height;
    }
    
    public int getWidth() {
        return width;
    }    

    public boolean hasBlockAt(int r, int c) {
        return CharMAP[r][c] == '#';
    }

    public boolean hasDotAt(int r, int c) {
        return CharMAP[r][c] == '.';

    }
    
    public boolean checkWord(int r,int c,char word){
    	return CharMAP[r][c] == word;
    }
    
    public boolean checkSpawn(int r,int c){
    	return MAP[numberOfMap][r].charAt(c) == 'S';
    }
    
    public void spawn(){
    	if(spawnX == 0){
    		for(int r = 0; r < getHeight(); r++) {
    			for(int c = 0; c < getWidth(); c++) {
    				if(checkSpawn(r, c)) {
    					this.spawnX = c * WorldRenderer.BLOCK_SIZE + WorldRenderer.BLOCK_SIZE/2;
    					this.spawnY = (r * WorldRenderer.BLOCK_SIZE) + WorldRenderer.BLOCK_SIZE - WorldRenderer.BLOCK_SIZE/2;
    				} 
    			}
    		}
    	}
    }
    
    public void changeMapToChar(){
    	for(int r = 0; r < height; r++) {
            for(int c = 0; c < width; c++) {
            	CharMAP[r][c] = MAP[numberOfMap][r].charAt(c); 
            }
    	}
    }
    
    public int getSpawnX(){
    	spawn();
        return spawnX;
       }
    
    public int getSpawnY(){
    	return spawnY;
    }
    
    public void changeBlock(int Row, int Col){
    	CharMAP[Row][Col]= '#';
    }
}