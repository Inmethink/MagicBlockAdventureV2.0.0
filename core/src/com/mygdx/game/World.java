package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

public class World {
	private Wizard wizard;
	private Block block;
	public BlockImg blockImg;
    private InvisibleMazeBattleGame invisibleMazeBatteGame;
    World(InvisibleMazeBattleGame invisibleMazeBatteGame) {
        this.invisibleMazeBatteGame = invisibleMazeBatteGame;
        block = new Block();
        block.changeMapToChar();
        blockImg = new BlockImg();
        wizard = new Wizard(block.getSpawnX(),block.getSpawnY(),this);
    }
    
    public void update(float delta) {
    	updateInputDirection();
        wizard.update();
    }
    
    private void updateInputDirection() {
		if(Gdx.input.isKeyPressed(Keys.W)) {
			wizard.setNextDirection(Wizard.DIRECTION_UP);
        }
		else if(Gdx.input.isKeyPressed(Keys.A)) {
			wizard.setNextDirection(Wizard.DIRECTION_LEFT);
        }
		else if(Gdx.input.isKeyPressed(Keys.S)) {
			wizard.setNextDirection(Wizard.DIRECTION_DOWN);
        } 
		else if(Gdx.input.isKeyPressed(Keys.D)) {
			wizard.setNextDirection(Wizard.DIRECTION_RIGHT);
        }
		else{
			wizard.setNextDirection(Wizard.DIRECTION_STILL);
		}
	}
    
    Wizard getWizard(){
    	return wizard;
    }
    
    Block getBlock() {
        return block;
    }
    
    BlockImg getBlockImg(){
    	return blockImg;
    }
    
}
