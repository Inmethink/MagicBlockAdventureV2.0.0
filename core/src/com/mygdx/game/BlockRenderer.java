package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class BlockRenderer {
	private BlockImg blockImg;
	private Block block;
    private SpriteBatch batch;
    private Texture blockImage;
    
    public BlockRenderer(SpriteBatch batch, World world) {
        this.blockImg = world.getBlockImg();
        this.block = world.getBlock();
        this.batch = batch;
        blockImage = new Texture("BlockInGame1.png");
    }
    
    public void render() {
//        for(int r = 0; r < blockImg.getHeight(); r++) {
//            for(int c = 0; c < blockImg.getWidth(); c++) {
//                int x = c * WorldRenderer.BLOCK_SIZE;
//                int y = MagicBlockAdventureGame.HEIGHT -(r * WorldRenderer.BLOCK_SIZE) - WorldRenderer.BLOCK_SIZE;
//                
//                if(blockImg.hasBlockAt(r, c)) {
//                    batch.draw(blockImage, x, y);
//                } 
//            }
//        }
        
        for(int r = 0; r < block.getHeight(); r++) {
            for(int c = 0; c < block.getWidth(); c++) {
                int x = c * WorldRenderer.BLOCK_SIZE;
                int y = InvisibleMazeBattleGame.HEIGHT -(r * WorldRenderer.BLOCK_SIZE) - WorldRenderer.BLOCK_SIZE;
                
                if(block.hasBlockAt(r, c)) {
                    batch.draw(blockImage, x, y);
                } 
            }
        }
    }

}
